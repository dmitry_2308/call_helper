from django.urls import include, path
from rest_framework.routers import DefaultRouter

from breaks.views.dicts import BreakStatusView, ReplacementStatusView

router = DefaultRouter()

router.register(r'dicts/remplacement-status', ReplacementStatusView, 'replacement_statuses')
router.register(r'dicts/break-status', BreakStatusView, 'break_statuses')

urlpatterns = [
]

urlpatterns += path('breaks/', include(router.urls)),
