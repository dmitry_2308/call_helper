from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class GroupInfo(models.Model):
    group = models.OneToOneField(
        to='organizations.Group',
        on_delete=models.CASCADE,
        related_name='break_info',
        verbose_name='Группа',
        primary_key=True
    )
    min_active_employees = models.PositiveSmallIntegerField(
        'Минимальное количество активных сотрудников',
        null=True,
        blank=True
    )
    break_start = models.TimeField('Время начала обеда', null=True, blank=True)
    break_end = models.TimeField('Время конца обеда', null=True, blank=True)
    break_max_duration = models.PositiveSmallIntegerField(
        'Максимальная длительность обеда',
        null=True, blank=True
    )

    class Meta:
        verbose_name = 'Параметр перерыва'
        verbose_name_plural = 'Параметры перерыва'

    def __str__(self):
        return f'{self.group}'


class Replacement(models.Model):
    name = models.CharField('Название', max_length=255)
    group = models.ForeignKey(
        to='breaks.GroupInfo',
        on_delete=models.CASCADE,
        related_name='replacements',
        verbose_name='Группа'
    )
    date = models.DateField('Дата смены')

    class Meta:
        verbose_name = 'Смена'
        verbose_name_plural = 'Смены'
        ordering = ('-date',)

    def __str__(self):
        return f'Смена № {self.pk} для {self.group}'


class ReplacementEmployee(models.Model):
    employee = models.ForeignKey(
        User, models.CASCADE, 'replacements', verbose_name='Сотрудник'
    )
    replacement = models.ForeignKey(
        'breaks.Replacement', models.CASCADE, 'employees', verbose_name='Смена'
    )
    status = models.ForeignKey(
        'breaks.ReplacementStatus', models.RESTRICT,
        'replacement_employees', verbose_name='Статус'
    )

    class Meta:
        verbose_name = 'Смена - Работник'
        verbose_name_plural = 'Смены - Работники'

    def __str__(self):
        return f'Смена {self.replacement} для {self.employee}'
