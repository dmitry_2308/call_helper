from django.contrib.auth import get_user_model
from django.db import models

from breaks.constants import BREAK_CREATED_STATUS
from breaks.models.dicts import BreakStatus

User = get_user_model()


class Break(models.Model):
    replacement = models.ForeignKey(
        to='breaks.Replacement',
        on_delete=models.CASCADE,
        related_name='breaks',
        verbose_name='Смена'
    )
    employee = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        related_name='breaks',
        verbose_name='Сотрудник'
    )
    break_start = models.TimeField('Время начала обеда', null=True, blank=True)
    break_end = models.TimeField('Время конца обеда', null=True, blank=True)
    status = models.ForeignKey(
        to='breaks.BreakStatus',
        on_delete=models.RESTRICT,
        related_name='breaks',
        verbose_name='Статус',
        null=False, blank=True
    )

    class Meta:
        verbose_name = 'Перерыв'
        verbose_name_plural = 'Перерывы'
        ordering = ('-replacement__date', 'break_start')

    def __str__(self):
        return f'Перерыв сотрудника {self.employee} из смены {self.replacement}'

    def save(self, *args, **kwargs):
        if not self.pk:
            self.status = BreakStatus.objects.get_or_create(code=BREAK_CREATED_STATUS)
        return super().save(*args, **kwargs)
