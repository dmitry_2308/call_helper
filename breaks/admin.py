from django.contrib import admin
from django.contrib.admin import TabularInline

from .models.breaks import Break
from .models.dicts import BreakStatus, ReplacementStatus
from .models.replacements import Replacement, ReplacementEmployee

admin.site.site_title = "Админ-панель"
admin.site.site_header = "Админ-панель"
admin.site.index_title = "Настройка сущностей"


class ReplacementEmployeeInline(TabularInline):
    model = ReplacementEmployee
    fields = ('employee', 'status',)


@admin.register(Replacement)
class ReplacementAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'name', 'group',
    )

    inlines = (ReplacementEmployeeInline,)


@admin.register(ReplacementStatus)
class ReplacementStatusAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'is_active')


@admin.register(BreakStatus)
class BreakStatusAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'is_active')


@admin.register(Break)
class BreakAdmin(admin.ModelAdmin):
    list_display = ('employee', 'replacement', 'break_start', 'break_end', 'status')
