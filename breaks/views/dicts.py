from drf_spectacular.utils import extend_schema, extend_schema_view

from breaks.models.dicts import BreakStatus, ReplacementStatus
from common.views.mixins import DictListMixin


@extend_schema_view(
    list=extend_schema(summary='Получение списка статусов смены', tags=['Словари']),
)
class ReplacementStatusView(DictListMixin):
    queryset = ReplacementStatus.objects.filter(is_active=True)


@extend_schema_view(
    list=extend_schema(summary='Получение списка статусов перерыва', tags=['Словари']),
)
class BreakStatusView(DictListMixin):
    queryset = BreakStatus.objects.filter(is_active=True)
