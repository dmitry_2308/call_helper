from django.contrib.auth import get_user_model
from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework.generics import CreateAPIView, RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.views import APIView

from common.views.mixins import ListViewSet
from users.permissions import IsNotCorporate
from users.serializers.api import users
from users.serializers.nested.users import UserShortSerializer

User = get_user_model()


@extend_schema_view(
    post=extend_schema(summary='Регистрация пользователя', tags=['Аутентификация и регистрация'])
)
class RegistrationView(CreateAPIView):
    queryset = User.objects.all()
    permission_classes = [AllowAny]
    serializer_class = users.RegistrationSerializer


@extend_schema_view(
    post=extend_schema(
        request=users.ChangePasswordSerializer,
        summary='Смена пароля', tags=['Аутентификация и регистрация']
    )
)
class ChangePasswordView(APIView):
    def post(self, request):
        user = request.user
        serializer = users.ChangePasswordSerializer(
            instance=user, data=request.data
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=HTTP_204_NO_CONTENT)


@extend_schema_view(
    get=extend_schema(summary='Получение профиля пользователя', tags=['Пользователи']),
    patch=extend_schema(summary='Частичное изменение профиля пользователя', tags=['Пользователи'])
)
class MeView(RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = users.MeSerializer
    http_method_names = ['get', 'patch']
    permission_classes = [IsNotCorporate]

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return users.MeUpdateSerializer
        return users.MeSerializer

    def get_object(self):
        return self.request.user


@extend_schema_view(
    list=extend_schema(summary='Получение списка пользователей', tags=['Пользователи']),
)
class UserListView(ListViewSet):
    queryset = User.objects.all()
    serializer_class = UserShortSerializer
