from django.contrib.auth import get_user_model

from common.serializers.mixins import ExtendedModelSerializer

User = get_user_model()


class UserShortSerializer(ExtendedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')
