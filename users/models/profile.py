from django.db import models


class Profile(models.Model):
    user = models.OneToOneField(
        'users.User', on_delete=models.CASCADE, primary_key=True,
        related_name='profile', verbose_name='Пользователь'
    )
    telegram_id = models.CharField('Telegram ID', max_length=64, blank=True, null=True, unique=True)

    class Meta:
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Профили пользователей'

    def __repr__(self):
        return f"{self.user}, tg: {self.telegram_id}"
