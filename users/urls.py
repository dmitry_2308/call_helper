from django.urls import include, path
from rest_framework.routers import DefaultRouter

from users.views.users import (ChangePasswordView, MeView, RegistrationView,
                               UserListView)

router = DefaultRouter()
router.register(r'', UserListView, 'users')

urlpatterns = [
    path('users/registration/', RegistrationView.as_view(), name='registration'),
    path('users/change-password/', ChangePasswordView.as_view(), name='change_password'),
    path('users/me', MeView.as_view(), name='me'),
]

urlpatterns += path('users/', include(router.urls)),
