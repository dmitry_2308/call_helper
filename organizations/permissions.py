from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsMyOrganization(BasePermission):
    def has_object_permission(self, request, view, obj):
        if obj.director == request.user:
            return True

        if request.method not in SAFE_METHODS:
            return obj.employees.all(user=request.user).exists()

        return False
