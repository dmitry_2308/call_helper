from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone

from common.models.mixins import InfoMixin

User = get_user_model()


class Group(InfoMixin, models.Model):
    organization = models.ForeignKey(
        to='Organization',
        on_delete=models.CASCADE,
        related_name='groups',
        verbose_name='Организация'
    )
    name = models.CharField('Название', max_length=255)
    manager = models.ForeignKey(
        to=User,
        on_delete=models.RESTRICT,
        related_name='groups_managers',
        verbose_name='Менеджер'
    )
    members = models.ManyToManyField(
        to=User,
        related_name='groups_members',
        verbose_name='Участники группы',
        blank=True, through='Member'
    )

    class Meta:
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'
        ordering = ('name',)

    def __str__(self):
        return f'{self.name}'


class Member(models.Model):
    group = models.ForeignKey('Group', models.CASCADE, 'members_info', verbose_name='Группа')
    user = models.ForeignKey(User, models.CASCADE, 'group_info', verbose_name='Сотрудник')
    position = models.ForeignKey('Position', models.RESTRICT, 'members', verbose_name='Позиция')
    date_joined = models.DateField('Дата начала работы', default=timezone.now)

    class Meta:
        verbose_name = 'Участник группы'
        verbose_name_plural = 'Участники группы'
        ordering = ('-date_joined',)
        unique_together = (('group', 'user'),)

    def __str__(self):
        return f'Member {self.user}'
