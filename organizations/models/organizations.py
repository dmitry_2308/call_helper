from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone

from common.models.mixins import InfoMixin

User = get_user_model()


class Organization(InfoMixin, models.Model):
    name = models.CharField('Название', max_length=255)
    director = models.ForeignKey(
        to=User,
        on_delete=models.RESTRICT,
        related_name='organizations_director',
        verbose_name='Директор'
    )
    employees = models.ManyToManyField(
        to=User,
        related_name='organizations_employees',
        verbose_name='Сотрудники',
        blank=True,
        through='Employee'
    )

    class Meta:
        verbose_name = 'Организациия'
        verbose_name_plural = 'Организациии'
        ordering = ('name',)

    def __str__(self):
        return f'{self.name}: {self.pk}'


class Employee(models.Model):
    organization = models.ForeignKey('Organization', models.CASCADE, 'employees_info', verbose_name='Организация')
    user = models.ForeignKey(User, models.CASCADE, 'organizations_info', verbose_name='Сотрудник')
    position = models.ForeignKey('Position', models.RESTRICT, 'employees', verbose_name='Позиция')
    date_joined = models.DateField('Дата начала работы', default=timezone.now)

    class Meta:
        verbose_name = 'Сотрудник организации'
        verbose_name_plural = 'Сотрудники организации'
        ordering = ('-date_joined',)
        unique_together = (('organization', 'user'),)

    def __str__(self):
        return f'Employee {self.user}'
