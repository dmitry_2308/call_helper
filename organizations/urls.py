from django.urls import include, path
from rest_framework.routers import DefaultRouter

from organizations.views.dicts import PositionView
from organizations.views.employees import EmployeeView
from organizations.views.groups import GroupView
from organizations.views.organizations import OrganizationView

router = DefaultRouter()

router.register(r'groups', GroupView, 'organizations_groups')
router.register(r'', OrganizationView, 'organizations')
router.register(r'(?P<pk>\d+)/employees', EmployeeView, 'organizations_employees')
router.register(r'dicts/positions', PositionView, 'positions')

urlpatterns = [
]

urlpatterns += path('organizations/', include(router.urls)),
