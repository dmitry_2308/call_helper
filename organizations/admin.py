from django.contrib import admin
from django.contrib.admin import TabularInline

from breaks.models.replacements import GroupInfo

from .models.dicts import Position
from .models.groups import Group, Member
from .models.organizations import Employee, Organization


class OrganizationEmployeeInline(TabularInline):
    model = Employee
    fields = ('user', 'position', 'date_joined')


class GroupMemberInline(TabularInline):
    model = Member
    fields = ('user', 'position', 'date_joined')


class GroupInfoInline(TabularInline):
    model = GroupInfo
    fields = (
        'min_active_employees',
        'break_start',
        'break_end',
        'break_max_duration',
    )


@admin.register(GroupInfo)
class GroupInfoAdmin(admin.ModelAdmin):
    list_display = (
        'group', 'min_active_employees', 'break_start',
        'break_end', 'break_max_duration'
    )


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'director',)
    list_display_links = ('id', 'director',)
    list_filter = ('director',)
    inlines = (OrganizationEmployeeInline,)
    readonly_fields = (
        'created_at', 'updated_at', 'created_by', 'updated_by',
    )


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'organization', 'manager',)
    inlines = (GroupMemberInline, GroupInfoInline)
    readonly_fields = (
        'created_at', 'updated_at', 'created_by', 'updated_by',
    )


@admin.register(Position)
class PositionAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active',)
