from django.db.models import Count
from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework.filters import OrderingFilter, SearchFilter

from common.views.mixins import CRUVViewSet
from organizations.models.groups import Group
from organizations.serializers.api.groups import GroupSerializer, GroupListSerializer


@extend_schema_view(
    list=extend_schema(summary='Получение списка групп', tags=['Организации: Группы']),
    create=extend_schema(summary='Создание группы', tags=['Организации: Группы']),
    retrieve=extend_schema(summary='Получение группы по id', tags=['Организации: Группы']),
    partial_update=extend_schema(summary='Частичное обновление групп', tags=['Организации: Группы']),
)
class GroupView(CRUVViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    http_method_names = ('get', 'post', 'patch')
    ordering = ('name', 'id')
    search_fields = ('name',)
    filter_backends = (
        OrderingFilter,
        SearchFilter,
    )

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return GroupListSerializer
        return GroupSerializer

    def get_queryset(self):
        print(self.action)
        queryset = Group.objects.prefetch_related(
            'organization',
        ).annotate(count_employess=Count('members'))

        return queryset
