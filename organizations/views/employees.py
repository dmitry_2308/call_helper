from drf_spectacular.utils import extend_schema, extend_schema_view

from common.views.mixins import CRUDViewSet
from organizations.models.organizations import Employee
from organizations.serializers.api.employees import EmployeeListSerializer, EmployeeCreateUpdateSerializer


@extend_schema_view(
    list=extend_schema(summary='Получение списка сотрудников организации', tags=['Организации: Сотрудники']),
    create=extend_schema(summary='Создание сотрудника организации', tags=['Организации: Сотрудники']),
    retrieve=extend_schema(summary='Получение сотрудника организации по id', tags=['Организации: Сотрудники']),
    partial_update=extend_schema(summary='Частичное обновление сотрудника организации', tags=['Организации: Сотрудники']),
    destroy=extend_schema(summary='Удаление сотрудника из организации', tags=['Организации: Сотрудники']),
)
class EmployeeView(CRUDViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeListSerializer
    lookup_url_kwarg = 'employee_id'
    http_method_names = ('get', 'post', 'patch', 'delete')

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return EmployeeListSerializer
        return EmployeeCreateUpdateSerializer

    def get_queryset(self):
        organization_id = self.request.parser_context['kwargs'].get('pk')
        queryset = Employee.objects.filter(organization_id=organization_id).select_related('position')
        return queryset
