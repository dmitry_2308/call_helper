from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework.filters import OrderingFilter, SearchFilter

from common.views.mixins import CRUVViewSet
from organizations.models.organizations import Organization
from organizations.permissions import IsMyOrganization
from organizations.serializers.api.ogranizations import (
    OrganizationCreateUpdateSerializer, OrganizationListSerializer)


@extend_schema_view(
    list=extend_schema(summary='Получение списка организаций', tags=['Организации']),
    create=extend_schema(summary='Создание организации', tags=['Организации']),
    retrieve=extend_schema(summary='Получение организации по id', tags=['Организации']),
    partial_update=extend_schema(summary='Частичное обновление организации', tags=['Организации']),
)
class OrganizationView(CRUVViewSet):
    permission_classes = [IsMyOrganization]
    queryset = Organization.objects.all()
    serializer_class = OrganizationListSerializer
    http_method_names = ('get', 'post', 'patch')
    ordering = ('name', 'id')
    search_fields = ('name',)
    filter_backends = (
        OrderingFilter,
        SearchFilter,
    )

    def get_queryset(self):
        queryset = Organization.objects.select_related(
            'director'
        ).prefetch_related(
            'employees',
            'groups'
        )
        return queryset

    def get_serializer_class(self):
        if self.action in ('retrieve', 'create', 'partial_update'):
            return OrganizationCreateUpdateSerializer
        return OrganizationListSerializer
