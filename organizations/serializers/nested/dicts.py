from common.serializers.mixins import ExtendedModelSerializer
from organizations.models.dicts import Position


class PositionShortSerializer(ExtendedModelSerializer):
    class Meta:
        model = Position
        fields = ('name', 'is_active')
