from rest_framework.exceptions import ParseError

from common.serializers.mixins import ExtendedModelSerializer
from organizations.models.organizations import Organization
from users.serializers.nested.users import UserShortSerializer


class OrganizationListSerializer(ExtendedModelSerializer):
    director = UserShortSerializer()

    class Meta:
        model = Organization
        fields = ('id', 'name', 'director', 'employees')


class OrganizationCreateUpdateSerializer(ExtendedModelSerializer):
    class Meta:
        model = Organization
        fields = ('id', 'name')

    @staticmethod
    def validate_name(value):
        org = Organization.objects.filter(name=value).first()
        if org:
            raise ParseError(
                f'Организация с именем `{value}` уже существует'
            )
        return value
