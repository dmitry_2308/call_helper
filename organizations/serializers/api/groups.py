from rest_framework import serializers

from common.serializers.mixins import ExtendedModelSerializer
from organizations.models.groups import Group
from organizations.serializers.api.ogranizations import \
    OrganizationListSerializer


class GroupListSerializer(ExtendedModelSerializer):
    organization = OrganizationListSerializer()
    count_employess = serializers.IntegerField()

    class Meta:
        model = Group
        fields = ('id', 'name', 'organization', 'count_employess')


class GroupSerializer(ExtendedModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name', 'organization')
