from api.spectacular.urls import urlpatterns as doc_urls
from breaks.urls import urlpatterns as breaks_urls
from organizations.urls import urlpatterns as organizations_urls
from users.urls import urlpatterns as user_urls

app_name = 'api'

urlpatterns = []

urlpatterns += doc_urls
urlpatterns += user_urls
urlpatterns += organizations_urls
urlpatterns += breaks_urls
