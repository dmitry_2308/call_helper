from crum import get_current_user
from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone

User = get_user_model()


class BaseDictModelMixin(models.Model):
    code = models.CharField('Код', max_length=16, primary_key=True)
    name = models.CharField('Название', max_length=64)
    is_active = models.BooleanField('Активность', default=True)

    class Meta:
        abstract = True
        ordering = ('code',)

    def __str__(self):
        return self.name


class DateMixin(models.Model):
    created_at = models.DateTimeField('Дата создания', null=True, blank=False)
    updated_at = models.DateTimeField('Дата обновления', null=True, blank=False)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.pk and not self.created_at:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()

        return super().save(*args, **kwargs)


class InfoMixin(DateMixin):
    created_by = models.ForeignKey(
        User, models.SET_NULL, 'created_%(app_label)s_%(class)s',
        null=True, verbose_name='Создал'
    )
    updated_by = models.ForeignKey(
        User, models.SET_NULL, 'updated_%(app_label)s_%(class)s',
        null=True, verbose_name='Обновил'
    )

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        user = get_current_user()
        if user and not user.pk:
            user = None
        if not self.pk:
            self.created_by = user
        self.updated_by = user
        super().save(*args, **kwargs)
